import React, { useEffect, useState } from "react";
import "./Main.css";
import Header from "../header/Header";
import Sort from "../header/Sort";
import Catalog from "../catalog/Catalog";
import CatSidebar from "../catalog/CatalogSideBar";
import Modal from "../Modal/Modal";
import { CircularProgress, Grid, makeStyles } from "@material-ui/core";
import {
  queryCategories,
  queryProductByCategory,
  queryProducts,
  querySingleProduct,
} from "../../API";
import {
  decodeUrlParam,
  encodeUrlParam,
  getLocalStorageItem,
  setLocalStorageItem,
} from "../utilities";
import { useHistory, useParams } from "react-router-dom";
import { addToCart } from "../server/server";

function Main() {
  const classes = useStyles();
  const { param } = useParams();

  const [loading, setLoading] = useState(false);
  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [singleProduct, setSingleProduct] = useState(null);
  const [sortState, setSortState] = useState("def");
  const [searchValue, setSearchValue] = useState("");
  const [qty, setQty] = useState(0);
  const handleSelect = (id) => {
    const foundId = selectedProducts.find((item) => item === id);
    const foundItem = products.find((item) => item.id === id);
    if (foundId) {
      const newArray = selectedProducts.filter((item) => item !== id);
      setSelectedProducts(newArray);
      foundItem.selected = false;
    } else {
      setSelectedProducts([...selectedProducts, id]);
      foundItem.selected = true;
    }
  };

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (!token) {
      history.push("./login");
    }
  }, [localStorage.hasOwnProperty("token")]);

  useEffect(() => {
    sortProducts(products, sortState);
  }, [searchValue]);

  const sortProducts = (filteredProducts, sortVal) => {
    const newData = [...filteredProducts];
    if (sortVal === "asc") {
      const sorted = newData.sort((a, b) => b.price - a.price);
      setProducts(sorted);
    } else if (sortVal === "desc") {
      const sorted = newData.sort((a, b) => a.price - b.price);
      setProducts(sorted);
    } else if (sortVal === "alpasc") {
      const sorted = newData.sort((a, b) => a.title.localeCompare(b.title));
      setProducts(sorted);
    } else if (sortVal === "alpdesc") {
      const sorted = newData.sort((a, b) => b.title.localeCompare(a.title));
      setProducts(sorted);
    } else {
      const sorted = newData.sort((a, b) => a.id - b.id);
      setProducts(sorted);
    }
  };

  useEffect(() => {
    setLoading(true);
    param &&
      queryProductByCategory(param).then((response) => {
        setProducts(response);
        setLoading(false);
      });
    queryProducts().then((response) => {
      const responseData = response.map((item) => ({
        ...item,
        selected: false,
      }));
      setLocalStorageItem("products", responseData);
      setProducts(responseData);
      setLoading(false);
    });
    queryCategories().then((response) => {
      const options = ["Choose Category", ...response];
      const resolvedOptions = options.map((item) => ({
        value: encodeUrlParam(item),
        label: item,
      }));
      setCategories(resolvedOptions);
      setLoading(false);
    });

    if (param) {
      querySingleProduct(param).then((item) => setSingleProduct(item));
    }
  }, []);

  const history = useHistory();
  const handleCategorySelect = (event) => {
    history.push(`/catalog/${event.target.value}`);
    const selectCategory = event.target.value;
    if (selectCategory === "Choose_Category") {
      setProducts(JSON.parse(getLocalStorageItem("products")));
    } else {
      const decodedValue = decodeUrlParam(selectCategory);
      queryProductByCategory(decodedValue).then((response) => {
        setProducts(response);
      });
    }
  };

  const handleOpen = (data) => {
    history.push(`/catalog/${data.id}`);
    setSingleProduct(data);
  };

  const handleClose = () => {
    setSingleProduct(null);
    setQty(0);
    history.goBack();
  };

  const selectAll = () => {
    setSelectedProducts(products.map((item) => item.id));
    products.map((item) => (item.selected = true));
  };
  const clearAll = () => {
    setSelectedProducts([]);
    products.map((item) => (item.selected = false));
  };

  const addCart = (productId, qty) => {
    addToCart(productId, qty || 1);
  };
  const increment = () => {
    setQty(() => qty + 1);
  };
  const decrement = () => {
    setQty(() => qty - 1);
  };
  return (
    <>
      {loading ? (
        <Grid container alignItems={"center"} justify={"center"}>
          <CircularProgress color={"primary"} />
        </Grid>
      ) : (
        <Grid container className={classes.main}>
          <Grid item lg={2} md={2}>
            <CatSidebar
              options={categories}
              onChange={(event) => handleCategorySelect(event)}
            />
          </Grid>
          <Grid item lg={10} md={10} sm={12} xs={12} className="content">
            <Header
              selectedCount={selectedProducts.length}
              count={products.length}
              fetchedProducts={JSON.parse(getLocalStorageItem("products"))}
              setData={setProducts}
              handleSelectAll={selectAll}
              handleClearAll={clearAll}
              setSearchValue={setSearchValue}
            />
            <Sort
              data={products}
              setSortState={setSortState}
              setData={setProducts}
              sortProducts={sortProducts}
            />
            <Grid container className="catalog">
              {products.map((item) => (
                <Grid item lg={3} key={item.id} md={4} xs={12} sm={6}>
                  <Catalog
                    handleSelect={handleSelect}
                    id={item.id}
                    key={item.id}
                    image={item.imageUrl}
                    title={item.title}
                    price={item.price}
                    handleOpen={() => handleOpen(item)}
                    selected={item.selected}
                    item={item}
                    handleAddToCart={() => addCart(item.id)}
                  />
                </Grid>
              ))}
            </Grid>
          </Grid>
          {singleProduct && (
            <Modal
              handleAddToCart={() => addCart(singleProduct.id, qty)}
              product={singleProduct}
              handleClose={handleClose}
              onIncrement={increment}
              onDecrement={decrement}
              qty={qty}
            />
          )}
        </Grid>
      )}
    </>
  );
}

const useStyles = makeStyles({
  main: {
    width: "100%",
  },
});

export default Main;
