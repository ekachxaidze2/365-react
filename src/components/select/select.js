import "./select.css";

export const Select = (props) => {
  return (
    <select className="choose__item--sort" onChange={props.onChange}>
      {props.options.map((option, index) => (
        <option
          className="choose__item--option"
          key={option.id || index}
          value={option?.value || option}
        >
          {option.label || option}
        </option>
      ))}
    </select>
  );
};
