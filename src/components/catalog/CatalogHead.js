import "./CatalogHead.css";
import Checkbox from "./Checkbox";
import Button from "../header/SelectButton";

const CatalogHead = ({
  catalogSelected,
  handleCatalogSelect,
  item,
  showDelete,
  handleAddToCart,
  handleDeleteFromCart,
}) => {
  return (
    <div
      className={`catalog__head ${
        catalogSelected ? "catalog__head--active" : ""
      }`}
    >
      <Checkbox
        checked={catalogSelected}
        handleCheckboxChange={handleCatalogSelect}
      />
      {showDelete ? (
        <Button handleClick={handleDeleteFromCart} title="Delete" />
      ) : (
        <Button handleClick={handleAddToCart} title="add to inventory" />
      )}
    </div>
  );
};
export default CatalogHead;
