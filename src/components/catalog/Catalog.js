import "./Catalog.css";
import CatalogHead from "./CatalogHead";

function Catalog({
  image,
  title,
  price,
  handleSelect,
  id,
  selected,
  handleOpen,
  item,
  qty,
  showDelete,
  handleAddToCart,
  handleDeleteFromCart,
}) {
  const checkboxClicked = () => {
    handleSelect(id);
  };

  const productClicked = () => {
    handleOpen({ id, image, title, price });
  };

  return (
    <div
      className={`catalog__product ${
        selected ? "catalog__product--border" : ""
      }`}
    >
      <CatalogHead
        handleAddToCart={handleAddToCart}
        handleDeleteFromCart={handleDeleteFromCart}
        catalogSelected={selected}
        handleCatalogSelect={checkboxClicked}
        item={item}
        showDelete={showDelete}
      />
      {qty && <p className="quantity">quantity: {qty}</p>}
      <div onClick={productClicked} className="catalog__product-body">
        <div className="catalog__img">
          <img src={image} alt="" />
        </div>
        <div className="product__item--middle">
          <div className="catalog__title">{title}</div>
        </div>
        <div className="product__item--price">
          <div className="catalog__prices">
            <span>RRP:</span> ${price}
          </div>
          <div className="catalog__prices">
            <span>Cost:</span> $654
          </div>
          <div className="catalog__prices">
            <span className="catalog__prices--modifier">Profit: 55%</span> / $4
          </div>
        </div>
      </div>
    </div>
  );
}

export default Catalog;
