import {Select} from "../select/select";
import "./CatalogSideBar.css";

const CatSidebar = (props) => {
  return (
    <section className="aside__item">
      <div className="choose__item choose__item--top">
        <span>Choose Niche</span> <i className="fas fa-angle-down"></i>
      </div>
      <div className="choose__item choose__item--bottom">
        <Select options={props.options} onChange={props.onChange}/>
        <i className="fas fa-angle-down"></i>
      </div>
    </section>
  );
};

export default CatSidebar;
