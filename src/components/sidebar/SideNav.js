import "./SideNav.css";
import Logo from "../header/Logo";
import NavLink from "../header/NavLink";

const SideNav = (props) => {
  return (
    <div className="SideBar__nav">
      <Logo/>
      <NavLink/>
    </div>
  )
}

export default SideNav;
