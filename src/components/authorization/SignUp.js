import React from "react";
import "./registration.css";
import logosvg from "../../icons/logo.svg";
import { Button, InputAdornment, Link, TextField } from "@material-ui/core";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import VpnKeyOutlinedIcon from "@material-ui/icons/VpnKeyOutlined";
import Checkbox from "@material-ui/core/Checkbox";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import FacebookIcon from "@material-ui/icons/Facebook";
import { useHistory } from "react-router-dom";
import BorderColorOutlinedIcon from "@material-ui/icons/BorderColorOutlined";
import { signUp } from "../server/server";
import * as yup from "yup";
import { useFormik } from "formik";

function SignUp() {
  const history = useHistory();

  const registrationSchema = yup.object({
    firstName: yup.string().required("Input Required"),
    lastName: yup.string().required("Input Required"),
    email: yup
      .string()
      .email("Invalid email address")
      .required("Input Required"),
    password: yup
      .string()
      .required("Password must be at least 6-20 characters long")
      .min(6, "Password must be at least 6-20 characters long")
      .max(20, "too long"),
    passwordConfirmation: yup
      .string()
      .required("Input Required")
      .oneOf([yup.ref("password"), null], "Passwords doesn`t match"),
  });
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      passwordConfirmation: "",
    },
    onSubmit: (values) => {
      signUp(
        values.firstName,
        values.lastName,
        values.email,
        values.password,
        values.passwordConfirmation
      )
        .then((result) => {
          checkToken(history);
        })
        .catch((err) => alert("Registration Error"));
    },
    validationSchema: registrationSchema,
  });

  const checkToken = (history) => {
    const token = localStorage.getItem("token");
    if (token) {
      history.push(`/login`);
    }
  };

  return (
    <div className="form__wrapper">
      <div className="form__authorization">
        <div className="form__header">
          <div className="form__img-wrapper">
            <img className="form__img" src={logosvg} alt="" />
          </div>
          <h2 className="form__heading">Sign UP</h2>
          <div></div>
        </div>

        <form onSubmit={formik.handleSubmit} className="authorization__form">
          <TextField
            placeholder="First Name"
            variant="outlined"
            name="firstName"
            id="firstName"
            value={formik.values.firstName}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.firstName && Boolean(formik.errors.firstName)}
            helperText={formik.touched.firstName && formik.errors.firstName}
            style={{ width: "100%", color: "grey", margin: "15px 0px" }}
            color="primary"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <BorderColorOutlinedIcon fontSize="small" color="primary" />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            placeholder="Last Name"
            name="lastName"
            id="lastName"
            variant="outlined"
            value={formik.values.lastName}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.lastName && Boolean(formik.errors.lastName)}
            helperText={formik.touched.lastName && formik.errors.lastName}
            style={{ width: "100%", color: "grey", margin: "15px 0px" }}
            color="primary"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <BorderColorOutlinedIcon fontSize="small" color="primary" />
                </InputAdornment>
              ),
            }}
          />

          <TextField
            placeholder="E-mail"
            name="email"
            id="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
            variant="outlined"
            style={{
              width: "100%",
              color: "grey",
              margin: "15px 0px",
              outline: "none",
            }}
            color="primary"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <MailOutlineIcon fontSize="small" color="primary" />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            placeholder="Password"
            name="password"
            id="password"
            variant="outlined"
            type="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
            style={{ width: "100%", color: "grey", margin: "15px 0px" }}
            color="primary"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <VpnKeyOutlinedIcon fontSize="small" color="primary" />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            placeholder="Confirm Password"
            name="passwordConfirmation"
            id="passwordConfirmation"
            variant="outlined"
            value={formik.values.passwordConfirmation}
            type="password"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={
              formik.touched.passwordConfirmation &&
              Boolean(formik.errors.passwordConfirmation)
            }
            helperText={
              formik.touched.passwordConfirmation &&
              formik.errors.passwordConfirmation
            }
            style={{ width: "100%", color: "grey", margin: "15px 0px" }}
            color="primary"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <VpnKeyOutlinedIcon fontSize="small" color="primary" />
                </InputAdornment>
              ),
            }}
          />

          <div className="privacy">
            By creating an account, you agree with the
            <a href="https://www.365dropship.com/terms-of-service/">
              Terms & Conditions
            </a>
            and
            <a href="https://www.365dropship.com/terms-of-service/">
              Privacy Policy
            </a>
          </div>
          <div className="inputs__subscribe">
            <Checkbox size="small" color="primary" />
            <span>Subscribe to Newsletter</span>
          </div>
          <Button variant="contained" color="primary" fullWidth type="submit">
            Sign Up
          </Button>
        </form>
        <div className="options">
          <h4 className="options__text">Or Log In With</h4>
        </div>
        <div className="registration__login">
          <button className="social">
            <LinkedInIcon fontSize="small" color="primary" />
          </button>
          <button className="social">
            <FacebookIcon fontSize="small" color="primary" />
          </button>
        </div>
        <div className="registration__redirect">
          <span>
            Already have an account? <Link to="/login">Sign in</Link>
          </span>
        </div>
      </div>
    </div>
  );
}

export default SignUp;
