import React from "react";
import "./registration.css";
import logosvg from "../../icons/logo.svg";
import { Button, InputAdornment, Link, TextField } from "@material-ui/core";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import VpnKeyOutlinedIcon from "@material-ui/icons/VpnKeyOutlined";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import FacebookIcon from "@material-ui/icons/Facebook";
import { useHistory } from "react-router-dom";
import { logIn } from "../server/server";
import * as yup from "yup";
import { useFormik } from "formik";

function LogIn() {
  const history = useHistory();

  const logInSchema = yup.object({
    email: yup
      .string()
      .email("Invalid email address")
      .required("Input Required"),
    password: yup
      .string()
      .required("Password must be at least 6-20 characters long")
      .min(6, "Password must be at least 6-20 characters long")
      .max(20, "too long"),
  });
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: (values) => {
      logIn(values.email, values.password)
        .then(history.push("./catalog"))
        .catch((err) => alert("error oe"));
    },
    validationSchema: logInSchema,
  });

  return (
    <>
      <div className="form__wrapper">
        <div className="form__authorization">
          <div className="form__header">
            <div className="form__img-wrapper">
              <img className="form__img" src={logosvg} alt="" />
            </div>
            <h2 className="form__heading">Members Log In</h2>
            <div></div>
          </div>
          <form onSubmit={formik.handleSubmit} className="authorization__form">
            <TextField
              placeholder="E-mail"
              name="email"
              id="email"
              value={formik.values.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
              variant="outlined"
              style={{
                width: "100%",
                color: "grey",
                margin: "15px 0px",
                outline: "none",
              }}
              color="primary"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <MailOutlineIcon fontSize="small" color="primary" />
                  </InputAdornment>
                ),
              }}
            />
            <TextField
              placeholder="Password"
              name="password"
              id="password"
              variant="outlined"
              type="password"
              value={formik.values.password}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
              style={{ width: "100%", color: "grey", margin: "15px 0px" }}
              color="primary"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <VpnKeyOutlinedIcon fontSize="small" color="primary" />
                  </InputAdornment>
                ),
              }}
            />

            <Button variant="contained" color="primary" fullWidth type="submit">
              Log in
            </Button>
          </form>
          <div className="options">
            <h4 className="options__text">Or Log In With</h4>
          </div>
          <div className="registration__login">
            <button className="social">
              <LinkedInIcon fontSize="small" color="primary" />
            </button>
            <button className="social">
              <FacebookIcon fontSize="small" color="primary" />
            </button>
          </div>
          <div className="registration__redirect">
            <span>
              Already have an account? <Link to="/login">Sign in</Link>
            </span>
          </div>
        </div>
      </div>
    </>
  );
}

export default LogIn;
