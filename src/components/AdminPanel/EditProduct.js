import React from "react";
import { updateProduct } from "../server/server";
import ProductForm from "./ProductForm";

const EditProduct = ({ product, onSuccess, handleClose }) => {
  return (
    <ProductForm
      product={product}
      onSubmit={async (data) => {
        const result = await updateProduct(data);
        onSuccess(result);
      }}
      handleClose={handleClose}
    />
  );
};

export default EditProduct;
