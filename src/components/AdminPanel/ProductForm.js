import React from "react";
import { useFormik } from "formik";
import "./ProductForm.css";

import { InputAdornment, TextField } from "@material-ui/core";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import ImageIcon from "@material-ui/icons/Image";
import SubjectIcon from "@material-ui/icons/Subject";
import TitleIcon from "@material-ui/icons/Title";
import Button from "@material-ui/core/Button";
import CloseIcon from "@material-ui/icons/Close";
import * as yup from "yup";

const ProductForm = ({ handleClose, product, onSubmit }) => {
  const addSchema = yup.object({
    title: yup.string().required("Input Required"),
    description: yup.string().required("Input Required"),
    price: yup
      .string()
      .required("Password must be at least 6-20 characters long"),
    imageUrl: yup.string().required("Input Required"),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: product
      ? {
          id: product.id,
          title: product.title,
          description: product.description,
          price: product.price,
          imageUrl: product.imageUrl,
        }
      : {
          title: "",
          description: "",
          price: "",
          imageUrl: "",
        },
    onSubmit: async (values) => {
      await onSubmit(values);
      alert("yey");
    },
    validationSchema: addSchema,
  });

  return (
    <div className="popup-box">
      <form onSubmit={formik.handleSubmit} className="add__products">
        <div className="form__header">
          <p>{product ? `Update` : "Add"} product</p>
          <CloseIcon className="close__popup" onClick={handleClose} />
        </div>
        <TextField
          placeholder="title"
          variant="outlined"
          name="title"
          id="title"
          value={formik.values.title}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.title && Boolean(formik.errors.title)}
          helperText={formik.touched.title && formik.errors.title}
          style={{ width: "100%", color: "grey", margin: "15px 0px" }}
          color="primary"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <TitleIcon fontSize="small" color="primary" />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          placeholder="description"
          variant="outlined"
          name="description"
          id="description"
          value={formik.values.description}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={
            formik.touched.description && Boolean(formik.errors.description)
          }
          helperText={formik.touched.description && formik.errors.description}
          style={{ width: "100%", color: "grey", margin: "15px 0px" }}
          color="primary"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SubjectIcon fontSize="small" color="primary" />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          placeholder="price"
          variant="outlined"
          name="price"
          id="price"
          value={formik.values.price}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.price && Boolean(formik.errors.price)}
          helperText={formik.touched.price && formik.errors.price}
          style={{ width: "100%", color: "grey", margin: "15px 0px" }}
          color="primary"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <AttachMoneyIcon fontSize="small" color="primary" />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          placeholder="imageUrl"
          variant="outlined"
          name="imageUrl"
          id="imageUrl"
          value={formik.values.imageUrl}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.imageUrl && Boolean(formik.errors.imageUrl)}
          helperText={formik.touched.imageUrl && formik.errors.imageUrl}
          style={{ width: "100%", color: "grey", margin: "15px 0px" }}
          color="primary"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <ImageIcon fontSize="small" color="primary" />
              </InputAdornment>
            ),
          }}
        />
        <Button variant="contained" color="primary" fullWidth type="submit">
          {product ? "Edit" : "Add"}
        </Button>
      </form>
    </div>
  );
};

export default ProductForm;
