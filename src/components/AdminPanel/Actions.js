import React, { useState } from "react";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import { makeStyles } from "@material-ui/core";
import AddProduct from "./AddProduct";
const Actions = () => {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);
  const togglePopup = () => {
    setIsOpen(!isOpen);
  };
  return (
    <div className={classes.wrapper}>
      <Fab
        onClick={togglePopup}
        className={classes.add}
        color="primary"
        aria-label="add"
        value="add"
      >
        <AddIcon />
      </Fab>
      {isOpen && <AddProduct handleClose={togglePopup} />}
      <Fab
        onClick={togglePopup}
        className={classes.edit}
        color="secondary"
        aria-label="edit"
        value="edit"
      >
        <EditIcon />
      </Fab>
    </div>
  );
};
const useStyles = makeStyles({
  wrapper: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  add: {
    marginRight: "20px",
    color: "#fff",
  },
  edit: {
    marginLeft: "20px",
  },
});
export default Actions;
