import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import AddProduct from "./AddProduct";
import EditProduct from "./EditProduct";
import EditIcon from "@material-ui/icons/Edit";
import AddIcon from "@material-ui/icons/Add";
import "./AdminPanel.css";
import { products } from "../server/server";
const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const AdminPanel = () => {
  const classes = useStyles();
  const [prods, setProducts] = useState([]);
  const [isAddFormOpen, setIsAddFormOpen] = useState(false);
  const [singleProduct, setSingleProduct] = useState(null);
  const [lastEditedProductResult, setLastEditedProductResult] = useState(null);
  const openAddFormModal = () => setIsAddFormOpen(true);
  const openEditFormModal = (product) => setSingleProduct(product);
  const closeAddFormModal = () => setIsAddFormOpen(false);
  const closeEditFormModal = () => setSingleProduct(null);
  const loadProducts = () => products().then((res) => setProducts(res));

  useEffect(() => {
    loadProducts();
  }, [lastEditedProductResult]);

  return (
    <>
      <div className="product__header">
        <h2>Product List</h2>
        <IconButton onClick={openAddFormModal} aria-label="add">
          <AddIcon />
        </IconButton>
        {isAddFormOpen && <AddProduct handleClose={closeAddFormModal} />}
        {singleProduct && (
          <EditProduct
            product={singleProduct}
            onSuccess={setLastEditedProductResult}
            handleClose={closeEditFormModal}
          />
        )}
      </div>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="right">Avatar</TableCell>
              <TableCell>title</TableCell>
              <TableCell align="left">Description</TableCell>
              <TableCell align="left">Price</TableCell>
              <TableCell align="right">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {prods.map((item) => (
              <TableRow key={item.id}>
                <TableCell align="right">
                  <Avatar alt="Remy Sharp" src={item.imageUrl} />
                </TableCell>
                <TableCell align="left">{item.title}</TableCell>
                <TableCell align="left">{item.description}</TableCell>
                <TableCell align="left">{item.price}</TableCell>

                <TableCell style={{ display: "flex" }} align="right">
                  <IconButton
                    onClick={() => openEditFormModal(item)}
                    aria-label="edit"
                    className={classes.margin}
                  >
                    <EditIcon />
                  </IconButton>
                  <IconButton aria-label="delete" className={classes.margin}>
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default AdminPanel;
