import React from "react";
import { addProduct } from "../server/server";
import ProductForm from "./ProductForm";

const AddProduct = ({ handleClose }) => {
  return <ProductForm onSubmit={addProduct} handleClose={handleClose} />;
};

export default AddProduct;
