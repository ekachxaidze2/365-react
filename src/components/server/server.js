import axios from "axios";

const SERVER_URL = "http://18.185.148.165:3000/";
const SERVER_URL_V1 = SERVER_URL + "api/v1/";

axios.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

export const signUp = async (
  firstName,
  lastName,
  email,
  password,
  passwordConfirmation
) => {
  try {
    const res = await axios.post(SERVER_URL + "register", {
      firstName,
      lastName,
      email,
      password,
      passwordConfirmation,
    });
    localStorage.setItem("user", JSON.stringify(res.data.data));
    localStorage.setItem("token", res.data.data.token);
  } catch (err) {
    throw new Error(err);
  }
};

export const logIn = async (email, password) => {
  try {
    const res = await axios.post(SERVER_URL + "login", {
      email,
      password,
    });
    localStorage.setItem("user", JSON.stringify(res.data.data));
    localStorage.setItem("token", res.data.data.token);
  } catch (err) {
    alert("erooooooor");
  }
};

export const cart = async () => {
  try {
    const res = await axios.get(SERVER_URL_V1 + "cart");
    return res.data.data;
  } catch (err) {
    if (err.response.status === 401) {
      localStorage.removeItem("token");
      localStorage.removeItem("user");
      window.location.href = "./login";
    }
  }
};

export const products = async () => {
  const res = await axios.get(SERVER_URL_V1 + "products");
  return res.data.data;
};
export const deleteFromCart = async (productId) => {
  const res = await axios.post(`${SERVER_URL_V1}cart/remove/${productId}`);
  return res.data.data;
};

export const addToCart = async (productId, qty) => {
  const res = await axios.post(SERVER_URL_V1 + "cart/add", {
    productId,
    qty,
  });
  return res.data.data;
};

export const addProduct = async (data) => {
  const res = await axios.post(SERVER_URL_V1 + "products", data);
  return res.data.data;
};

export const getProduct = async (id) => {
  const res = await axios.post(SERVER_URL_V1 + `products/${id}`);
  return res.data.data;
};

export const updateProduct = async (data) => {
  const res = await axios.put(SERVER_URL_V1 + `products/${data.id}`, data);
  return res.data.data;
};

export const editCart = async (id, qty) => {
  const res = await axios.post(SERVER_URL_V1 + `cart/update/${id}`, { qty });
  return res.data.data;
};
