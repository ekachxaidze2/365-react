import "./HelpMenu.css"

const HelpMenu = () => {
  return (
    <i className="far fa-question-circle header__mark"></i>
  )
}
export default HelpMenu;
