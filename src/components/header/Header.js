import "./Header.css";
import Button from "./SelectButton";
import HelpMenu from "./HelpMenu";
import SearchInput from "./SearchInput";
import { useHistory } from "react-router-dom";

function Header({
  selectedCount,
  count,
  fetchedProducts,
  setData,
  handleClearAll,
  handleSelectAll,
  setSearchValue,
}) {
  const history = useHistory();
  const logOut = () => {
    localStorage.removeItem("token");
    history.push("./login");
  };
  return (
    <div className="header">
      <div className="header__item">
        <Button handleClick={handleSelectAll} title="SELECT ALL" />
        <span className="header__selection">{`selected ${selectedCount} out of ${count} products`}</span>
        {selectedCount > 0 ? (
          <Button handleClick={handleClearAll} title="CLEAR ALL" />
        ) : (
          ""
        )}
      </div>
      <div className="header__item">
        <SearchInput
          placeholder={"search..."}
          fetchedProducts={fetchedProducts}
          setData={setData}
          setSearchValue={setSearchValue}
        />
        <Button title="add to inventory" big />
        <HelpMenu />
        <p onClick={logOut}>Log out</p>
      </div>
    </div>
  );
}

export default Header;
