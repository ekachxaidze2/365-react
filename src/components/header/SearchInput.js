import {useState} from "react";
import {useParams} from "react-router-dom";

import "./SearchInput.css";

const SearchInput = ({
                       fetchedProducts,
                       setData,
                       placeholder,
                       setSearchValue,
                     }) => {
  const {category} = useParams();
  const [input, setInput] = useState("");
  const handleSearch = (e) => {
    setSearchValue(input);
    e.preventDefault();
    const result = fetchedProducts.filter((item) => {
      if (category !== "Choose Category" && !!category) {
        return (
          item.category === category &&
          item.title.toLowerCase().includes(input.toLowerCase())
        );
      } else {
        return item.title.toLowerCase().includes(input.toLowerCase());
      }
    });
    setData(result);
  };

  return (
    <form className="header__form" onSubmit={handleSearch}>
      <input
        id="searchQuery"
        className="header__input"
        type="text"
        value={input}
        placeholder={placeholder}
        onChange={(e) => setInput(e.target.value)}
      />
      <button id="searchButton" className="header__searchbtn" type="submit">
        <i className="fas fa-search fa-rotate-90"></i>
      </button>
    </form>
  );
};
export default SearchInput;
