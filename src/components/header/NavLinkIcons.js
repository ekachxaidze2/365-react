export const NavLinkIcons = [
  {
    iconName: "fas fa-user-astronaut",
    name: "profile",
  },
  {
    iconName: "fas fa-tachometer-alt",
    name: "dashboard",
  },
  {
    iconName: "fas fa-bars",
    name: "catalog",
  },
  {
    iconName: "fas fa-cube",
    name: "AdminPanel",
  },
  {
    iconName: "fas fa-shopping-cart",
    name: "cart",
  },
  {
    iconName: "fas fa-clipboard-check",
    name: "order",
  },
  {
    iconName: "fas fa-exchange-alt",
    name: "transactions",
  },
  {
    iconName: "fas fa-list-alt",
    name: "list",
  },
];
