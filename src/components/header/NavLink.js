import {Link} from "react-router-dom";
import "./NavLink.css";
import {NavLinkIcons} from "./NavLinkIcons";

const NavLink = (props) => {
  return (
    <ul className="SideBar__list">
      {NavLinkIcons.map((icon, index) => (
        <li key={index} className="SideBar__list--item">
          <Link to={`/${icon.name}`}>
            <i className={`${icon.iconName} iconolor`}></i>
          </Link>
        </li>
      ))}
    </ul>
  );
};
export default NavLink;
