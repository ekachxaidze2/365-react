import React from "react";
import "./LandingPage.css";
import {Link} from "react-router-dom";

export const LandingPage = () => {
  return (
    <div className="Landing__page">
      <div className="Landing__header">
        <img
          className="Landing__header-logo"
          src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/group-30.png"
          alt="dropshipLogo"
        />
        <div className="header__nav">
          <Link className="header__nav-item" to="/about">
            <span className="header__nav-item">ABOUT</span>
          </Link>
          <Link className="header__nav-item" to="/catalog">
            <span className="header__nav-item">CATALOG</span>
          </Link>
          <Link className="header__nav-item" to="/pricing">
            <span className="header__nav-item">PRICING</span>
          </Link>
          <Link className="header__nav-item" to="/suppliers">
            <span className="header__nav-item">SUPPLIERS</span>
          </Link>
          <Link className="header__nav-item" to="/help">
            <span className="header__nav-item">HELP CENTER</span>
          </Link>
          <Link className="header__nav-item" to="/blog">
            <span className="header__nav-item">BLOG</span>
          </Link>
          <Link className="header__nav-item" to="/signup">
            <button className="header__nav-button-bordered">SIGN UP NOW</button>
          </Link>
          <Link className="header__nav-item" to="/login">
            <button className="header__nav-button">LOGIN</button>
          </Link>
          <i className="fab fa-facebook-f facebook"/>
        </div>
      </div>
      <div className="Landing__main">
        <img
          src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg"
          alt="dropshipLogo"
        />
        <h3 className="Landing__title">WE GOT YOUR SUPPLY CHAIN COVERED</h3>
        <h3 className="Landing__title">365 DAYS A YEAR!</h3>
      </div>
      <Link className="header__nav-item" to="/signup">
        <button className="SignUp__Button">SIGN UP NOW</button>
      </Link>
    </div>
  );
};
