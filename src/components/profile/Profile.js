import {useState} from "react";
import axios from "axios";

export const Profile = () => {
  const SERVICE_URL = "http://18.185.148.165:3000";

  const [email, setEmail] = useState("");

  const [password, setPassword] = useState("");

  const login = () => {
    axios
      .post(SERVICE_URL + "login", {
        email: email,
        password: password,
      })
      .then((user) => {
        localStorage.setItem("user", JSON.stringify(user));
        localStorage.setItem("token", user.data.data.token);
      });
  };

  return (
    <div className="profile">
      <input
        type="text"
        name="firstName"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      ></input>
      <input
        type="text"
        name="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      ></input>
      <input type="submit" value="Login" onClick={login}></input>
    </div>
  );
};
