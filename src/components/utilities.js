export const setLocalStorageItem = (key, value) => {
  if (typeof value === "object") {
    localStorage.setItem(key, JSON.stringify(value))
  } else {
    localStorage.setItem(key, value)
  }
}

export const getLocalStorageItem = key => {
  return localStorage.getItem(key)
}

// takes 2 arguments event for excess input value and callback function which is called for this value
export function setInputValue(event, callback) {
  callback(event.target.value)
}

export function encodeUrlParam(item) {
  return item.split(' ').join('_')
}

export function decodeUrlParam(item) {
  return item.split('_').join(' ')
}
