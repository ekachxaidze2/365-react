import "./App.css";
import SideBar from "./components/sidebar/SideBar";
import Main from "./components/main/Main";
import { Route, Switch } from "react-router";
import { Grid, makeStyles } from "@material-ui/core";
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { LandingPage } from "./components/LandingPage/LandingPage";
import LogIn from "./components/authorization/LogIn";
import SignUp from "./components/authorization/SignUp";
import Cart from "./Cart/Cart";
import AdminPanel from "./components/AdminPanel/AdminPanel";

function App() {
  const classes = useStyles();
  const location = useLocation();

  const [renderSideBar, setRenderSideBar] = useState(false);

  useEffect(() => {
    if (
      location.pathname === "/" ||
      location.pathname === "/login" ||
      location.pathname === "/signup"
    ) {
      setRenderSideBar(false);
    } else {
      setRenderSideBar(true);
    }
  }, [location, renderSideBar]);

  return (
    <Grid container className={classes.root}>
      {renderSideBar && (
        <Grid item className={classes.sidebar}>
          <SideBar />
        </Grid>
      )}

      <Grid container item className={renderSideBar ? classes.main : ""}>
        <Switch>
          <Route exact path="/">
            <LandingPage />
          </Route>
          <Route path="/profile">
            <p>I need some sleep</p>
          </Route>
          <Route path="/dashboard">
            <p>You can't go home like this</p>
          </Route>
          <Route path="/catalog/:param?">
            <Main />
          </Route>
          <Route path="/AdminPanel" component={AdminPanel}></Route>
          <Route path="/cart" component={Cart}></Route>
          <Route path="/order">
            <p>I need some sleep</p>
          </Route>
          <Route path="/transactions">
            <p>Time to put the old horse down</p>
          </Route>
          <Route path="/list">
            <p>I'm in too deep</p>
          </Route>
          <Route path="/login" component={LogIn}></Route>
          <Route path="/signup" component={SignUp}></Route>
        </Switch>
      </Grid>
    </Grid>
  );
}

const useStyles = makeStyles({
  root: {
    backgroundColor: "#F8F9FA",
    minHeight: "100vh",
    height: "100%",
    width: "100%",
  },
  sidebar: {
    width: "55px",
  },
  main: {
    width: "calc(100% - 55px)",
  },
  "@media only screen and (max-width: 970px)": {
    sidebar: {
      width: "0px",
    },
    main: {
      width: "100%",
    },
  },
});

export default App;
