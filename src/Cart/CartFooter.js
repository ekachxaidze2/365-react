import React from "react";
import Button from "@material-ui/core/Button";
import "./CartFooter.css";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import { makeStyles } from "@material-ui/core";

const CartFooter = () => {
  const classes = useStyles();

  return (
    <div className="cart__footer">
      <div className="cart__checkout">
        <Button className={classes.shoping} variant="contained" color="primary">
          <KeyboardBackspaceIcon />
          continue shopping
        </Button>
      </div>
      <div className="footer__data">
        <div>
          BALANCE:<span className="price">$0</span>
        </div>
        <div>
          ITEMS TOTAL:<span className="price">$0</span>
        </div>
        <div>
          SHIPPING TOTAL:<span className="price">$0</span>
        </div>
        <div>
          ORDER TOTAL:<span className="price">$0</span>
        </div>
        <Button
          className={classes.checkout}
          variant="contained"
          color="primary"
        >
          CHECKOUT
        </Button>
      </div>
    </div>
  );
};
const useStyles = makeStyles({
  checkout: {
    color: "#fff",
    width: "168px",
    height: "38px",
    borderRadius: "4px",
    border: "none",
    fontWeight: "600",
    marginLeft: "15px",
    cursor: "pointer",
  },
  shoping: {
    padding: "10px 22px",
    bordeRadius: "4px",
    textTransform: "uppercase",
    fontSize: "14px",
    fontWeight: "600",
    color: "#f9fbfe",
  },
});
export default CartFooter;
