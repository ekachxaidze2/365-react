import "./CartItem.css";
import DeleteIcon from "@material-ui/icons/Delete";
import { TableRow, TableCell } from "@material-ui/core";

import QuantityQounter from "./QuantityQounter";

const CartItem = ({
  image,
  title,
  quantity,
  price,
  deleteFromCart,
  onIncrement,
  onDecrement,
}) => {
  return (
    <TableRow>
      <TableCell component="th">
        <div className="cntr">
          <img src={image} alt="prodImg"></img>
          <span className="diff title">{title}</span>
        </div>
      </TableCell>
      <TableCell align="center">
        <span className="supplier">SP-Supplier115</span>
      </TableCell>
      <TableCell align="left">
        <div className="qty">
          <QuantityQounter
            onIncrement={onIncrement}
            onDecrement={onDecrement}
            qty={quantity}
          />
        </div>
      </TableCell>
      <TableCell align="center">
        <span className="cost">{price}$</span>
      </TableCell>
      <TableCell align="center">
        <DeleteIcon
          fontSize="small"
          color="secondary"
          onClick={deleteFromCart}
        />
      </TableCell>
    </TableRow>
  );
};
export default CartItem;
