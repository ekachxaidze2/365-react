import "./CartHeader.css";
import HelpOutlineOutlinedIcon from "@material-ui/icons/HelpOutlineOutlined";

const CartHeader = () => {
  return (
    <div className="cart__header">
      <span>SHOPPING CART()</span>
      <div className="header__about">
        <HelpOutlineOutlinedIcon fontSize="medium" color="secondary" />
      </div>
    </div>
  );
};

export default CartHeader;
