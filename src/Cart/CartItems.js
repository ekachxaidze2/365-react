import "./CartItems.css";
import {
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Paper,
  makeStyles,
} from "@material-ui/core";

import CartItem from "./CartItem";
import { editCart } from "../components/server/server";
import { useState } from "react";
const useStyles = makeStyles(() => ({
  container: {
    paddingRight: 40,
    paddingLeft: 40,
    boxSizing: "border-box",
  },
}));

const CartItems = ({ products, setTriggerChange, deleteFromCart }) => {
  const classes = useStyles();
  const [prods, setProducts] = useState(products);
  const increment = (item) => {
    editCart(item.id, item.qty + 1).then((res) => {
      setProducts(res.cartItem.items);
    });
  };

  const decrement = (item) => {
    editCart(item.id, item.qty - 1);
  };
  return (
    <div className="cart__items">
      <TableContainer className={classes.container} component={Paper}>
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>
                <span className="diff">ITEM DESCRIPTION</span>
              </TableCell>
              <TableCell align="center">
                <span className="diff">SUPPLIER</span>
              </TableCell>
              <TableCell align="center">
                <span className="diff">QUANTITY</span>
              </TableCell>
              <TableCell align="center">
                <span className="diff">ITEM COST</span>
              </TableCell>
              <TableCell align="center">
                <span className="diff">DELETE</span>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {products.length > 0
              ? products.map((item, i) => {
                  return (
                    <CartItem
                      image={item.image}
                      title={item.title}
                      quantity={item.qty}
                      price={item.price}
                      id={item.id}
                      key={i}
                      setTriggerChange={setTriggerChange}
                      deleteFromCart={() => deleteFromCart(item.id)}
                      onIncrement={() => increment(item)}
                      onDecrement={() => decrement(item)}
                    />
                  );
                })
              : null}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};
export default CartItems;
