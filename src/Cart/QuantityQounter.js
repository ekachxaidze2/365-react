import React from "react";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";

const QuantityQounter = ({ qty = 1, onIncrement, onDecrement }) => {
  return (
    <ButtonGroup size="small" aria-label="small outlined button group">
      {<Button onClick={onIncrement}>+</Button>}
      {<Button disabled>{qty}</Button>}

      <Button disabled={qty <= 1} onClick={onDecrement}>
        -
      </Button>
    </ButtonGroup>
  );
};

export default QuantityQounter;
