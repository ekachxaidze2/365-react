import "./Cart.css";
import { useEffect, useState } from "react";
import CartHeader from "./CartHeader";
import CartFooter from "./CartFooter";
import CartItems from "./CartItems";
import { cart, deleteFromCart } from "../components/server/server";
const Cart = () => {
  const [products, setCartProducts] = useState([]);
  const [triggerChange, setTriggerChange] = useState(false);

  useEffect(() => {
    getCartProducts();
  }, [triggerChange]);

  const getCartProducts = () => {
    cart().then((res) => {
      setCartProducts(res.cartItem.items);
    });
  };

  const deleteItemFromCart = (productId) => {
    deleteFromCart(productId).then(() => cart().then(getCartProducts));
  };

  return (
    <div className="cart">
      <CartHeader />
      <CartItems
        deleteFromCart={deleteItemFromCart}
        products={products}
        setTriggerChange={setTriggerChange}
      />
      <CartFooter />
    </div>
  );
};

export default Cart;
