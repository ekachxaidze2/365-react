import axios from "axios";

export const BASE_URl = "http://18.185.148.165:3000/";
export const queryData = async (url) => {
  const request = await axios.get(`${BASE_URl}${url}`);
  return request.data.data;
};

export const queryProducts = async () => await queryData("api/v1/products");

export const querySingleProduct = async (id) =>
  await queryData(`products/${id}`);

export const queryCategories = async () =>
  await queryData("products/categories");

export const queryProductByCategory = async (category) =>
  await queryData(`products/category/${category}`);
